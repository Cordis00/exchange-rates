<?php declare(strict_types = 1);

namespace App\Controllers;

use App\Services\CurrencyService;
use Http\Request;
use Http\Response;
use App\Template\Renderer;

class Homepage
{
	private $request;
	private $response;
	private $renderer;
	private $currencyService;

	public function __construct(
		Request $request,
		Response $response,
		Renderer $renderer
	) {
		$this->request = $request;
		$this->response = $response;
		$this->renderer = $renderer;

		$this->currencyService = new CurrencyService();
	}

	public function show()
	{
		$currencyList = $this->currencyService->getCurrencies();
		$data = [
			'currencyList' => $currencyList
		];
		$html = $this->renderer->render('Homepage', $data);
		$this->response->setContent($html);
	}

	public function calc()
	{
		$amount = $this->request->getParameter('amount');
		$from = $this->request->getParameter('from');
		$to = $this->request->getParameter('to');

		$result = $this->currencyService->calculate($from, $to, $amount);
		$this->response->setContent($result);
	}
}