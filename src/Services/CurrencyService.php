<?php

namespace App\Services;

use Exception;

class CurrencyService
{
	private $apiUrl;
	private $apiKey;

	public function __construct()
	{
		$this->apiUrl = 'https://free.currconv.com/api/v7/';
		$this->apiKey = 'do-not-use-this-key';
	}

	public function getCurrencies()
	{
		$currencyList = [];
		try {
			$req_url = $this->apiUrl . 'currencies?apiKey=' . $this->apiKey;
			$response_json = file_get_contents($req_url);
			$currencyList = (array) json_decode($response_json)->results;
		}
		catch(Exception $e) {
			print_r($e->getMessage());
		}
		return $currencyList;
	}

	public function calculate($from, $to, $amount)
	{
		$result = 0;
		try {
			$currencyName = $from . '_' . $to;
			$req_url = $this->apiUrl . 'convert?q=' . $currencyName . '&apiKey=' . $this->apiKey;
			$response_json = file_get_contents($req_url);
			$currencyValue = json_decode($response_json)->results->{$currencyName}->val;
			$result = $currencyValue * $amount;
		}
		catch(Exception $e) {
			print_r($e->getMessage());
		}
		return $result;
	}
}