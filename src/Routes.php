<?php declare(strict_types = 1);

return [
	['GET', '/', ['App\Controllers\Homepage', 'show']],
	['POST', '/calc', ['App\Controllers\Homepage', 'calc']],
	['GET', '/history', ['App\Controllers\History', 'show']],
	['GET', '/settings', ['App\Controllers\Settings', 'show']],
];