let headerLinks = document.querySelectorAll('header li a');

for (let i = 0; i < headerLinks.length; i++) {
	if(headerLinks[i].href == window.location.href) {
		headerLinks[i].classList.add('active')
	}
}

document.getElementById('calculate-button').addEventListener('click', function() {
	let xmlhttp = new XMLHttpRequest()
	xmlhttp.onreadystatechange = function() {
		if (xmlhttp.readyState == XMLHttpRequest.DONE) {
			if (xmlhttp.status == 200) {
				document.getElementById('result').innerHTML = xmlhttp.responseText;
			}
			else if (xmlhttp.status == 400) {
				alert('There was an error 400');
			}
			else {
				alert('something else other than 200 was returned');
			}
		}
	};

	xmlhttp.open('POST', '/calc', true)
	xmlhttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
	let fromCurency = document.getElementById('from').value
	let toCurency = document.getElementById('to').value
	let amount = document.getElementById('amount').value
	let data = 'from=' + fromCurency + '&to=' + toCurency + '&amount=' + amount
	xmlhttp.send(data)
})